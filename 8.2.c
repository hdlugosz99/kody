#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

float srednia(float *tab){
    float suma = 0;

    for(int i=0;i<50;i++)
        suma += tab[i];

    return suma / 50;
}

float odch_stand(float *tab){
    float tmp = 0;
    float wart_sr = srednia(tab);

    for(int i=0; i<50; i++)
        tmp += powf(tab[i] - wart_sr, 2);

float wart_odch_stand = sqrtf(tmp/50);
return wart_odch_stand;
}

void sortowanie(float *tab){
	float tmp;
    int rozmiar = 50;

	for(int i=0; i<rozmiar; i++){
		for(int j=1; j<rozmiar-i; j++){
            if(*(tab + j-1)>*(tab + j)){
                tmp = *(tab + j-1);
                *(tab + j-1) = *(tab + j);
                *(tab + j) = tmp;
            }
        }
    }

    return;
}

float f_mediana(float *tab){  //deklaracja i definicja funkcji obliczajacej mediane
    float mediana;
    float tmp;
    int rozmiar = 50;

    if(rozmiar % 2 == 0){
       tmp = *(tab + (rozmiar/2)-1) + *(tab + (rozmiar/2));
       mediana = tmp/2;
       }
    else if(rozmiar % 2 != 0){
       mediana = *(tab + (rozmiar/2));
       }

return mediana;
}

struct dane
{
    float iksy[50];
    float igreki[50];
    float erhao[50];

}dane;

struct wyniki
{
float srednia, mediana, odchylenie;
}wyniki1[3];

int main()
{
    FILE *plik;
    plik = fopen("P0001_attr.txt", "r");

    char tmp[4];

    for(int i = 0; i<4; i++)
    {
        fscanf(plik, "%s", tmp);
    }

    for(int i = 0; i <50; i++)
    {
        fscanf(plik, "%s", tmp);
        fscanf(plik, "%f", &dane.iksy[i]);
        fscanf(plik, "%f", &dane.igreki[i]);
        fscanf(plik, "%f", &dane.erhao[i]);
    }

    int find_result = 0;
    char temp[512];

    while(fgets(temp, 512, plik) != NULL)
    {
		if((strstr(temp, "Mediana:")) != NULL)
        {
			find_result++;
		}
	}

	fclose(plik);

    if(find_result == 0)
    {
    plik = fopen("P0001_attr.txt", "a");

    fprintf(plik ,"\n");

    sortowanie(dane.iksy);
    wyniki1[1].mediana = f_mediana(dane.iksy);
    fprintf(plik, "Mediana: %.2f\t\t", f_mediana(dane.iksy));

    sortowanie(dane.igreki);
    wyniki1[2].mediana = f_mediana(dane.igreki);
    fprintf(plik, "Mediana: %.2f\t\t", f_mediana(dane.igreki));

    sortowanie(dane.erhao);
    wyniki1[3].mediana = f_mediana(dane.erhao);
    fprintf(plik, "Mediana: %.2f", f_mediana(dane.erhao));

    fprintf(plik ,"\n");

    wyniki1[1].srednia = srednia(dane.iksy);
    fprintf(plik,"Srednia: %.3f\t\t", srednia(dane.iksy));
    wyniki1[2].srednia = srednia(dane.igreki);
    fprintf(plik,"Srednia: %.3f\t\t", srednia(dane.igreki));
    wyniki1[3].srednia = srednia(dane.erhao);
    fprintf(plik,"Srednia: %.3f", srednia(dane.erhao));

    fprintf(plik ,"\n");
    wyniki1[1].odchylenie = odch_stand(dane.iksy);
    fprintf(plik,"Odchylenie: %.3f\t", odch_stand(dane.iksy));
    wyniki1[2].odchylenie = odch_stand(dane.igreki);
    fprintf(plik,"Odchylenie: %.3f\t", odch_stand(dane.igreki));
    wyniki1[3].odchylenie = odch_stand(dane.erhao);
    fprintf(plik,"Odchylenie: %.3f", odch_stand(dane.erhao));

    fclose(plik);
   }
   else
    printf("Wyniki sa juz wpisane!\n");
    return 0;
}
