#include <stdio.h>
#include <stdlib.h>
#include <string.h>


struct zawodnik // deklaracja struktury zawodnika
{
    char imie[50];
    char nazwisko[50];
    char narodowosc[50];
    char rekord[50];
};

void z_pliku_do_struktur(FILE *plik, struct zawodnik biegacz[3]); // deklaracja procedur
void wypisanie(struct zawodnik biegacz[3]);
void zmiana_rekordu(struct zawodnik biegacz[3]);
void nadpisanie(FILE *plik, struct zawodnik biegacz[3]);

int main()
{
    FILE *plik;
    plik = fopen("plik.txt", "r"); // otwarcie pliku w trybie odczytu

    struct zawodnik biegacz[3]; // deklaracja tablicy skladajacej sie z trzech zmiennych strukturalnych

    printf("Najszybsi zawodnicy w biegu na 5000m na hali w historii: \n\n");

    z_pliku_do_struktur(plik, biegacz);

    wypisanie(biegacz);

    fclose(plik); // zamkniecie pliku

    plik = fopen("plik.txt", "w"); // otwarcie pliku w trybie zapisu

    zmiana_rekordu(biegacz);

    nadpisanie(plik, biegacz);

    fclose(plik); // zamkniecie pliku

    return 0;
}


void z_pliku_do_struktur(FILE *plik, struct zawodnik biegacz[3]) // definicja procedury pobierajacej zawartosc z pliku i wpisujacej do odpowiednich pol zmiennych strukturalnych
{
    for(int i=0; i<3; i++){

        fscanf(plik, "%s", biegacz[i].imie);
        fscanf(plik, "%s", biegacz[i].nazwisko);
        fscanf(plik, "%s", biegacz[i].narodowosc);
        fscanf(plik, "%s", biegacz[i].rekord);
    }

    return;
}


void wypisanie(struct zawodnik biegacz[3]) // definicja procedury wypisujacej dane zawodnikow
{
    for(int i=0; i<3; i++){

        printf("%d. ", i+1);
        printf("%s ", biegacz[i].imie);
        printf("%s ", biegacz[i].nazwisko);
        printf("%s ", biegacz[i].narodowosc);
        printf("%s ", biegacz[i].rekord);
        printf("\n\n");
    }

    return;
}


void zmiana_rekordu(struct zawodnik biegacz[3]) // definicja procedury zmieniajacej czas wybranego zawodnika przez uzytkownika
{
    printf("Wybierz zawodnika w celu poprawienia jego czasu (1, 2 lub 3): \n");

    int wybor = 0;

    do{
        scanf("%d", &wybor);
    }while(wybor != 1 && wybor != 2 && wybor != 3);

    printf("Wpisz nowy czas zawodnika %d. w formacie min:sek:\n", wybor);
    scanf(" %s", &biegacz[wybor-1].rekord);

    printf("Nowy rekord: %s\n\n", biegacz[wybor-1].rekord);

    return;
}


void nadpisanie(FILE *plik, struct zawodnik biegacz[3]) // definicja procedury wpisujacej do pliku zaktualizowane dane
{
    for(int j=0;j<3;j++){

        fprintf(plik, "%s ", biegacz[j].imie);
        fprintf(plik, "%s ", biegacz[j].nazwisko);
        fprintf(plik, "%s ", biegacz[j].narodowosc);
        fprintf(plik, "%s ", biegacz[j].rekord);
        fprintf(plik,"\n");

    }

    return;
}
