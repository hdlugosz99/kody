#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>

float srednia(int tab[]){
    float suma = 0;

    for(int i=0;i<50;i++)
        suma += tab[i];

    return suma / 50;
}

float odch_stand(int tab[]){
    float tmp = 0;
    float wart_sr = srednia(tab);

    for(int i=0; i<50; i++)
        tmp += powf(tab[i] - wart_sr, 2);

float wart_odch_stand = sqrtf(tmp/50);
return wart_odch_stand;
}

int main(){

int tab[50];
srand(time(NULL));

for(int i = 0; i<50; i++){
    tab[i] = 1 + rand() % 6;
    //printf("%d. %d\n", i+1, tab[i]); //wypisywanie kolejnych wylosowanych liczb
}

printf("Wartosc srednia wynosi: %.3f\n", srednia(tab));
printf("Odchylenie standardowe wynosi: %.3f\n", odch_stand(tab));
return 0;
}
