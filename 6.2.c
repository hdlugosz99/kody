#include <stdio.h>
#include <stdlib.h>

void kopiowanie(FILE *plik_1, FILE *plik_2); // deklaracja procedury przenoszacej tresc miedzy plikami
int wybor_f(); // deklaracja funkcji umozliwiajacej wybor sposobu przekopiowania danych

int main()
{
    FILE *plik_1, *plik_2;

    plik_1 = fopen("plik_1.txt", "r"); // otwarcie pliku w trybie 'r' pozwalajacym na odczytanie jego tresci

    int wybor = wybor_f();

    if(wybor == 1){
        plik_2 = fopen("plik_2.txt", "a"); // otwarcie drugiego pliku w trybie 'a' pozwalajacym na dopisanie tresci do pliku
        kopiowanie(plik_1, plik_2); // procedura przenoszenia tresci miedzy plikami
    }
    else{
        plik_2 = fopen("plik_2.txt", "w"); // otwarcie drugiego pliku w trybie 'w' pozwalajacym na nadpisanie tresci w pliku
        kopiowanie(plik_1, plik_2); // procedura przenoszenia tresci miedzy plikami
    }

    // zamkniecie plikow
    fclose(plik_1);
    fclose(plik_2);

    return 0;
}


void kopiowanie(FILE *plik_1, FILE *plik_2) // definicja procedury przenoszacej tresc miedzy plikami
{
    char ch = ' ';

    if(plik_1 == NULL || plik_2 == NULL){ // warunek sprawdzajacy czy pliki istnieja
        printf("Problem z odczytem plikow.");
        exit(1);
    }

    while( ( ch = fgetc(plik_1) ) != EOF ){ // przenoszenie tresci przy pomocy funkcji fgetc() i fputc()
        fputc(ch, plik_2);
    }

    return;
}

int wybor_f() // definicja funkcji umozliwiajacej wybor sposobu przekopiowania danych
{
    printf("Wybierz w jaki sposob chcesz skopiowac tresc jednego pliku do drugiego wpisujac odpowiednia cyfre:\n");
    printf(" - dopisac do pliku - 1\n - nadpisac plik - 2\n");

    int wybor = 0;
    do{
        scanf("%d", &wybor);
    }while(wybor != 1 && wybor != 2);

    return wybor;
}

