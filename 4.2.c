#include <stdio.h>
#include <stdlib.h>

void przekatne(double **wsk, int rozmiar){ // Deklaracja i definicja procedury uzupelniajacej przekatna tablicy wartosciami losowymi
    srand(time(NULL));

    for(int i=0; i<rozmiar; i++){
        wsk[i][i] = (rand() % 2000)/100.f;
    }

    return;
}

double slad(double **wsk, int rozmiar){  // Deklaracja i definicja funkcji obliczajacej slad tablicy
    double slad = 0;

    for(int i=0; i<rozmiar; i++){
        slad += wsk[i][i];
    }

    return slad;
}

void wypisywanie(double **wsk, int rozmiar){ // Deklaracja i definicja procedury wypisujacej wszystkie wartosci tablicy
    for(int i=0; i<rozmiar; i++){
        for(int j=0; j<rozmiar; j++){
            printf("%5.2lf  ", wsk[i][j]);
        }
    printf("%\n\n\n");
    }

    return;
}

int main()
{
    int rozmiar;
    printf("Podaj liczbe calkowita, ktora bedzie okreslala wielkosc tablicy kwadratowej 2D: \n");
    scanf("%d", &rozmiar);

    // alokacja tablicy o wymiarach podanych wyzej
    double **wsk;
    wsk = (double **)calloc(rozmiar, sizeof(double *));
    for(int k=0; k<rozmiar; k++)
        wsk[k] = (double*)calloc(rozmiar, sizeof(double));

    // Wywolanie wszystkich funkcji
    przekatne(wsk, rozmiar);
    printf("Slad macierzy wynosi: %.2lf \n\n", slad(wsk, rozmiar));
    wypisywanie(wsk, rozmiar);

    // Zwolnienie tablicy
    for (int k=0; k<rozmiar; k++)
        free(wsk[k]);
    free(wsk);
    return 0;
}
