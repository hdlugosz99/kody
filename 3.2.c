#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>

int max(int x, int y, int tab[x][y]){  //deklaracja i definicja funkcji znajdujacej wartosc maksymalna
    int wart_max = tab[0][0];

    for(int i=0; i<x; i++){
        for(int j=0; j<y; j++){
            if(wart_max < tab[i][j])
                wart_max = tab[i][j];
    }}

    return wart_max;
}



int min(int x, int y, int tab[x][y]){  //deklaracja i definicja funkcji znajdujacej wartosc minimalna
    int wart_min = tab[0][0];

    for(int i=0; i<x; i++){
        for(int j=0; j<y; j++){
            if(wart_min > tab[i][j])
                wart_min = tab[i][j];
    }}

    return wart_min;
}



int main(){
    int x;
    int y;
    srand(time(NULL));

    do {    //wpisywanie liczby wierszy przez uzytkownika z przedzialu <1;10>
        printf("Podaj liczbe wierszy (liczba od 1 do 10): \n");
        scanf("%d", &x);
    } while(x>10 || x<1);

    do {    //wpisywanie liczby kolumn przez uzytkownika z przedzialu <1;10>
        printf("Podaj liczbe kolumn (liczba od 1 do 10): \n");
        scanf("%d", &y);
    } while(y>10 || y<1);

    printf("liczba wierszy: %d  liczba kolumn: %d\n\n", x, y);

    int tab[x][y];

    for(int i=0; i<x; i++){ //uzupelnianie tablicy losowymi liczbami z przedzialu <1;1000>
        for(int j=0; j<y; j++){
            tab[i][j] = 1 + rand() % 1000;
            printf("%5d  ", tab[i][j]);
        }
        printf("\n\n");
    }

    printf("Wartosc maksymalna tablicy wynosi: %5d\n", max(x, y, tab)); //wywolanie funkcji
    printf("Wartosc minimalna tablicy wynosi: %5d\n", min(x, y, tab));  //wywolanie funkcji

    return 0;
}

