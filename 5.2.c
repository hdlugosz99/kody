#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

int sprawdzenie(char p_dlugosc[]){ // deklaracja i definicja funkcji sprawdzajacej czy podany lancuch znakow sklada sie z samych liczb typu int
    int ilosc_cyfr = 0;
    int liczba = 0;

    for(int i=0; i<strlen(p_dlugosc); i++){ //sprawdzenie czy string sklada sie z samych cyfr
        if(isdigit(p_dlugosc[i]))
            ilosc_cyfr += 1;
    }

    if(ilosc_cyfr == strlen(p_dlugosc)){ // jesli sklada sie z samych cyfr to jest konwertowany na int
        liczba = atoi(p_dlugosc); // konwersja lancuch na liczbe typu int
    }

    return liczba;
}

void sila_hasla(char haslo1[]){ // deklaracja i definicja procedury sprawdzajacej silnosc hasla
    int duza = 0;
    int mala = 0;
    int cyfra = 0;
    int znak_specjalny = 0;


    for(int i=0; i<strlen(haslo1); i++){ //sprawdzenie warunkow silnosci hasla
        if(islower(haslo1[i]))
            mala = 1;
        else if(isupper(haslo1[i]))
            duza = 1;
        else if(isdigit(haslo1[i]))
            cyfra = 1;
        else if((haslo1[i] >= 33 && haslo1[i] <= 47) || (haslo1[i] >= 58 && haslo1[i] <= 64))
            znak_specjalny = 1;
    }

    if(duza == 1 && mala == 1 && cyfra == 1 && znak_specjalny == 1) // jesli warunki spelnione - wypisana zostaje informacja
        printf("Haslo silne!\n");

    return;
}


int main()
{
    char dlugosc[10];
    int liczba = 0;


    do{
        printf("Podaj liczbe calkowita z zakresu <10;50>: \n");
        scanf(" %[^\n]s", &dlugosc);
        liczba = sprawdzenie(dlugosc);
    } while(liczba > 50 || liczba < 10); // sprawdzanie poprawnosci wpisanego lancucha i ewentualne powtorzenie petli

    char haslo1[liczba];
    char haslo2[liczba];

    do{
        printf("Wymysl i wpisz haslo(maksymalnie %d znakow): ", liczba);
        scanf(" %[^\n]s", &haslo1);
        printf("Wpisz ponownie haslo: ");
        scanf(" %[^\n]s", &haslo2);

        if ( strcmp( haslo1, haslo2 ) != 0 ) // sprawdzenie czy obydwa stringi sa identyczne
            printf("\nHasla sie nie zgadzaja, ponow probe. \n\n");

    }while( strcmp( haslo1, haslo2 ) != 0 ); // uzycie funkcji porownujacej dwa stringi

    sila_hasla(haslo1);

    return 0;
}
