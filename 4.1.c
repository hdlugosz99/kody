#include <stdio.h>
#include <stdlib.h>
#include <time.h>


// Dopiero robiac drugie zadanie zorientowalem sie ze nie musialem dzialac na wskaznikach tak jak w poprzednim zestawie (*(wsk + 1)).
// Zdecydowalem ze zostawie to tak jak bylo.

void losowanie(int *wsk, int rozmiar){ //deklaracja i definicja procedury losujacej wartosci i uzupelniajacej nimi tablice
    srand(time(NULL));

    for(int i=0; i<rozmiar; i++){
        *(wsk + i) = 5 + rand() % 46;
    }

    printf("\n\n");
    return;
}

void sortowanie(int *wsk, int rozmiar){ //deklaracja i definicja procedury sortujacej tablice przy pomocy sortowania babelkowego
	int tmp;

	for(int i=0; i<rozmiar; i++){
		for(int j=1; j<rozmiar-i; j++){
            if(*(wsk + j-1)>*(wsk + j)){
                tmp = *(wsk + j-1);
                *(wsk + j-1) = *(wsk + j);
                *(wsk + j) = tmp;
            }
        }
    }

    return;
}

float f_mediana(int *wsk, int rozmiar){  //deklaracja i definicja funkcji obliczajacej mediane
    float mediana;
    float tmp;

    if(rozmiar % 2 == 0){
       tmp = *(wsk + (rozmiar/2)-1) + *(wsk + (rozmiar/2));
       mediana = tmp/2;
       }
    else if(rozmiar % 2 != 0){
       mediana = *(wsk + (rozmiar/2));
       }

return mediana;
}

void wyswietlanie(int *wsk, int rozmiar){ //deklaracja i definicja procedury wyswietlajacej wszystkie elementy tablicy
    for(int i=0; i<rozmiar; i++)
        printf("%3d. %3d\n", i+1, *(wsk + i));

    return;
}

int main(){
    int rozmiar;
    printf("Podaj liczbe calkowita, ktora bedzie okreslala wielkosc tablicy 1D: \n");
    scanf("%d", &rozmiar);

    int *wsk;
    wsk = (int *)malloc(rozmiar * sizeof(int));

    losowanie(wsk, rozmiar);
    sortowanie(wsk, rozmiar);
    wyswietlanie(wsk, rozmiar);
    printf("\nMediana jest rowna: %.2f\n", f_mediana(wsk, rozmiar));

    free(wsk);
    return 0;
}
