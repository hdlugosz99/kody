#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

float los_x(); // deklaracja funkcji losujacych liczby
float los_y();
float los_rho();

int main()
{
    srand(time(NULL));
    FILE *plik;

    printf("Wpisz ciag znakow, ktory bedzie podstawa do nazwania plikow(maksymalnie 15 znakow): \n");
    char nazwa_tmp[16] = ""; // zmienna, ktora bedzie przechowywala czesc nazwy pliku wpisana przez uzytkownika
    scanf(" %15[^\n]s",&nazwa_tmp);

    for(int i = 1; i <= 5; i++){

        char nazwa[45] = ""; // zmienna, ktora bedzie przechowywala pelna nazwe pliku
        snprintf(nazwa, 45, "P000%d_", i); //uzycie funkcji, ktora pozwala zawrzec w nazwie pliku cyfre
        strcat(nazwa, nazwa_tmp);  // tworzenie nazwy pliku przy pomocy funkcji laczacej lancuchy znakow
        strcat(nazwa, ".rec");
        printf("%s \n", nazwa);

        plik = fopen(nazwa, "w"); //otwarcie pliku w trybie 'write' o nazwie stworzonej wyzej

        //wpisanie do pliku tresci:
        fprintf(plik, "LP.\tX\tY\t\tRHO\n");

        for(int i = 1; i <= 50; i++){
            fprintf(plik, "%d.\t%.5f\t%.5f\t%.3f\n", i, los_x(), los_y(), los_rho()); //uzycie funkcji losujacych liczby i wpisywanie ich do pliku
        }

        fclose(plik); // 'zamkniecie' pliku
    }
    printf("Program zakonczyl dzialanie. ");

    return 0;
}

// definicje funkcji:
float los_x(){
    float liczba_x = ( (float)rand() / (float)RAND_MAX ) * 10.0;
    return liczba_x;
}


float los_y(){
    float liczba_y = 10.0 + ( (float)rand() / (float)RAND_MAX ) * 40.0;
    return liczba_y;
}


float los_rho(){
    float liczba_rho = 2.7 + ( (float)rand() / (float)RAND_MAX ) * 0.14;
    return liczba_rho;
}
