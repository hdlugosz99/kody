#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>

void zeros(int p_tab[][10][10]){ //deklaracja i definicja procedury uzupelniajacej tablice zerami
    for(int i=0; i<10; i++){
        for(int j=0; j<10; j++){
            for(int k=0; k<10; k++){
                p_tab[i][j][k] = 0;
            }
        }
    }
    return;
}
void fifties(int p_tab[][10][10]){   //deklaracja i definicja procedury wypelniajacej tablice "50"
    for(int i=0; i<10; i++){
        p_tab[i][i][i] = 50;
    }
    return;
}

int wypisywanie(int tab[][10][10]){   //deklaracja i definicja funkcji wypisujacej tablice
for(int i=0;i<10;i++)
    {
    printf("\n\n");
    for(int j=0;j<10;j++)
        {
        printf("\n");
        for(int k=0;k<10;k++)
            {
            printf("%2d ",tab[i][j][k]);
        }
    }
}
return 0;
}

int main()
{
int tab[10][10][10];

zeros(tab);
fifties(tab);
wypisywanie(tab);

return 0;
}

