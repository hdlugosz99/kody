#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

void konwersja_liter(char p_tekst[]){ // deklaracja i definicja procedury zmieniajacej wielkosc liter
    char znak;

    for(int i=0; i < strlen(p_tekst) ; i++){
        znak = p_tekst[i];
        if(islower(znak)){
            p_tekst[i] = toupper(p_tekst[i]);
        }
        if(isupper(znak)){
            p_tekst[i] = tolower(p_tekst[i]);
        }
    }
    printf("Twoj tekst po konwersji: %s\n\n", p_tekst);

    return;
}

int ile_cyfr(char p_tekst[]){ // deklaracja i definicja funkcji zliczajacej ilosc cyfr w lancuchu
    int ilosc = 0;

    for(int i=0; i < strlen(p_tekst) ; i++){
        if(isdigit(p_tekst[i])){
            ilosc += 1;
        }
    }

    return ilosc;
}


int main()
{
    char imie[20];
    char tekst[41];
    int dlugosc = 0;

    printf("Podaj swoje imie: ");
    scanf("%s", &imie);
    printf("Witaj %s!\n", imie);
    printf("Napisz cos(maksymalnie 40znakow): \n");
    scanf(" %40[^\n]s",&tekst);
    printf("\nTwoj tekst przed konwersja: %s\n\n", tekst);
    konwersja_liter(tekst);
    printf("W podanym tekscie znajdowalo sie: %d cyfr", ile_cyfr(tekst));

    return 0;
}
