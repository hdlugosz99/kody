#include <stdio.h>
#include <stdlib.h>
#include <math.h>

float srednia(float *tab){
    float suma = 0;

    for(int i=0;i<50;i++)
        suma += tab[i];

    return suma / 50;
}

float odch_stand(float *tab){
    float tmp = 0;
    float wart_sr = srednia(tab);

    for(int i=0; i<50; i++)
        tmp += powf(tab[i] - wart_sr, 2);

float wart_odch_stand = sqrtf(tmp/50);
return wart_odch_stand;
}

void sortowanie(float *tab){
	float tmp;
    int rozmiar = 50;

	for(int i=0; i<rozmiar; i++){
		for(int j=1; j<rozmiar-i; j++){
            if(*(tab + j-1)>*(tab + j)){
                tmp = *(tab + j-1);
                *(tab + j-1) = *(tab + j);
                *(tab + j) = tmp;
            }
        }
    }

    return;
}

float f_mediana(float *tab){  //deklaracja i definicja funkcji obliczajacej mediane
    float mediana;
    float tmp;
    int rozmiar = 50;

    if(rozmiar % 2 == 0){
       tmp = *(tab + (rozmiar/2)-1) + *(tab + (rozmiar/2));
       mediana = tmp/2;
       }
    else if(rozmiar % 2 != 0){
       mediana = *(tab + (rozmiar/2));
       }

return mediana;
}


int main()
{
    FILE *plik;
    plik = fopen("P0001_attr.rec.txt", "r");

    float *iksy;
    iksy = (float *)malloc(50 * sizeof(float));

    float *igreki;
    igreki = (float *)malloc(50 * sizeof(float));

    float *erhao;
    erhao = (float *)malloc(50 * sizeof(float));

    char tmp[4];

    for(int i = 0; i<4; i++)
    {
        fscanf(plik, "%s", tmp);
    }

    for(int i = 0; i <50; i++)
    {
        fscanf(plik, "%s", tmp);
        fscanf(plik, "%f", &iksy[i]);
        fscanf(plik, "%f", &igreki[i]);
        fscanf(plik, "%f", &erhao[i]);
    }

    fclose(plik);
    plik = fopen("P0001_attr.rec.txt", "a");

    fprintf(plik ,"\n");

    sortowanie(iksy);
    fprintf(plik, "Mediana: %.2f\t\t", f_mediana(iksy));

    sortowanie(igreki);
    fprintf(plik, "Mediana: %.2f\t\t", f_mediana(igreki));

    sortowanie(erhao);
    fprintf(plik, "Mediana: %.2f", f_mediana(erhao));

    fprintf(plik ,"\n");

    fprintf(plik,"Srednia: %.3f\t\t", srednia(iksy));
    fprintf(plik,"Srednia: %.3f\t\t", srednia(igreki));
    fprintf(plik,"Srednia: %.3f", srednia(erhao));

    fprintf(plik ,"\n");

    fprintf(plik,"Odchylenie: %.3f\t", odch_stand(iksy));
    fprintf(plik,"Odchylenie: %.3f\t", odch_stand(igreki));
    fprintf(plik,"Odchylenie: %.3f", odch_stand(erhao));

    fclose(plik);
    return 0;
}

/*

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

float srednia(float *tab){
    float suma = 0;

    for(int i=0;i<50;i++)
        suma += tab[i];

    return suma / 50;
}

float odch_stand(float *tab){
    float tmp = 0;
    float wart_sr = srednia(tab);

    for(int i=0; i<50; i++)
        tmp += powf(tab[i] - wart_sr, 2);

float wart_odch_stand = sqrtf(tmp/50);
return wart_odch_stand;
}

void sortowanie(float *tab){ //deklaracja i definicja procedury sortujacej tablice przy pomocy sortowania babelkowego
	float tmp;
    int rozmiar = 50;

	for(int i=0; i<rozmiar; i++){
		for(int j=1; j<rozmiar-i; j++){
            if(*(tab + j-1)>*(tab + j)){
                tmp = *(tab + j-1);
                *(tab + j-1) = *(tab + j);
                *(tab + j) = tmp;
            }
        }
    }

    return;
}

float f_mediana(float *tab){  //deklaracja i definicja funkcji obliczajacej mediane
    float mediana;
    float tmp;
    int rozmiar = 50;

    if(rozmiar % 2 == 0){
       tmp = *(tab + (rozmiar/2)-1) + *(tab + (rozmiar/2));
       mediana = tmp/2;
       }
    else if(rozmiar % 2 != 0){
       mediana = *(tab + (rozmiar/2));
       }

return mediana;
}


int main()
{
    FILE *plik;
    plik = fopen("P0001_attr.rec.txt", "r");

    float *iksy;
    iksy = (float *)malloc(50 * sizeof(float));

    float *igreki;
    igreki = (float *)malloc(50 * sizeof(float));

    float *erhao;
    erhao = (float *)malloc(50 * sizeof(float));

    char tmp[4];

    for(int i = 0; i<4; i++)
    {
        fscanf(plik, "%s", tmp);
    }

    for(int i = 0; i <50; i++)
    {
        fscanf(plik, "%s", tmp);
        fscanf(plik, "%f", &iksy[i]);
        printf("%f ", iksy[i]);
        fscanf(plik, "%f", &igreki[i]);
        printf("%f ", igreki[i]);
        fscanf(plik, "%f", &erhao[i]);
        printf("%f \n", erhao[i]);
    }


    printf("IKSY");
    sortowanie(iksy);
    printf("\nMediana jest rowna: %.2f\n", f_mediana(iksy));
    printf("Wartosc srednia wynosi: %.3f\n", srednia(iksy));
    printf("Odchylenie standardowe wynosi: %.3f\n", odch_stand(iksy));

    printf("IGREKI");
    sortowanie(igreki);
    printf("\nMediana jest rowna: %.2f\n", f_mediana(igreki));
    printf("Wartosc srednia wynosi: %.3f\n", srednia(igreki));
    printf("Odchylenie standardowe wynosi: %.3f\n", odch_stand(igreki));

    printf("ERHAO");
    sortowanie(erhao);
    printf("\nMediana jest rowna: %.2f\n", f_mediana(erhao));
    printf("Wartosc srednia wynosi: %.3f\n", srednia(erhao));
    printf("Odchylenie standardowe wynosi: %.3f\n", odch_stand(erhao));

    for(int i = 0; i <50; i++)
    {

        printf("%d. %f \n",i+1, iksy[i]);
    }

    for(int i = 0; i <50; i++)
    {

        printf("%d. %f \n",i+1, igreki[i]);
    }

    for(int i = 0; i <50; i++)
    {

        printf("%d. %f \n",i+1, erhao[i]);
    }



    return 0;
}

*/




