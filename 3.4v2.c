#include <stdio.h>
#include <stdlib.h>
#include <stdlib.h>

void mnozenie(int tab[][20]){
    int mnoznik;
    do {
        printf("\n\nWprowadz liczbe od 2 do 50: ");
        scanf("%d", &mnoznik);
    } while(mnoznik > 50 || mnoznik < 2);

    for(int i=0; i<20; i++){
        for(int j=0; j<20; j++){
            tab[i][j] *= mnoznik;
        }
    }
return;
}

void dodawanie(int p_tab1[][20], int p_tab2[][20]){

    for(int i=0; i<20; i++){
        for(int j=0; j<20; j++){
            p_tab1[i][j] += p_tab2[i][j];
        }
    }

return;
}

void wyswietlanie(int tab[][20]){
    printf("\n\n");
    for(int i=0; i<20; i++){
        printf("\n");
        for(int j=0; j<20; j++){
            printf("%2d ", tab[i][j]);
        }
    }
return;
}

int main(){
    int tab_1[20][20];
    int tab_2[20][20];

    for(int i=0; i<20; i++){
        for(int j=0; j<20; j++){
            tab_1[i][j] = 1;
            tab_2[i][j] = 1;
        }
    }
    printf("Pierwsza Tablica: ");
    wyswietlanie(tab_1);
    printf("\nDruga Tablica: ");
    wyswietlanie(tab_2);
    mnozenie(tab_1);
    printf("\nPrzemnozona Tablica: ");
    wyswietlanie(tab_1);
    printf("\nPrzemnozona Tablica dodac Druga Tablica: ");
    dodawanie(tab_1, tab_2);
    wyswietlanie(tab_1);

    return 0;
}
