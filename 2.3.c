#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <math.h>

#define M 9

bool wiekszemniejsze(int f_a, int f_b){
    bool w;
    if (f_a > f_b)
        {
        w = true;
        printf("Tak, %d > %d\n", f_a, f_b);
        }
    else
    {
        w = false;
        printf("Nie, %d < lub = %d\n", f_a, f_b);
        return false;
    }
    return w;
}

float iloczyn(int f_a, int f_b, bool w){
    int i = 0;
    float iloczyn = 1.0;

    if( w == true || f_a == f_b ) // jesli a > b lub a == b
    {
        for(i=0; i<M; i++)
        {
            if((i % 2) == 0) // tylko dla parzystych i
                iloczyn *= pow((i+1), 2) + M;
        }
    }
    else // jesli b < a
    {
        for(i=0; i<M; i++)
        {
            if((i % 2) != 0) // tylko dla nieparzystych i
                iloczyn *= pow((i+1), 2) + M;
        }
    }
    return iloczyn;
    }

void pierw_z_kw_iloczynu(float f_iloczyn)
{
    float wynik = 0;
    wynik = sqrt(pow(f_iloczyn, 2)); // pierwiastek z kwadratu iloczynu
    printf("Wynik operacji = %.4f\n", wynik);

}

int main()
{
    int a = 0, b = 0;

    printf("Podaj dwie liczby calkowite:\n");

    scanf("%d %d", &a, &b);

	printf("Czy %d > %d ?\n", a, b);

    bool w = wiekszemniejsze(a, b);

    float x = iloczyn(a, b, w);

    printf("%f", x);

    pierw_z_kw_iloczynu(x);

	return 0;
}
