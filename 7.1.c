#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define PI 3.1415926

struct zespolona // deklaracja struktury liczby zespolonej
{
    float Re;
    float Im;
    float modul;
    float Arg;
};

void wczytanie(char wariant, struct zespolona *liczba); //deklaracja procedur
void wypisanie(char wariant, struct zespolona *liczba);
void przeliczanie(char wariant, struct zespolona *liczba);

int main()
{

    struct zespolona liczba = {0.0, 0.0, 0.0, 0.0};
    char wariant = '0';

    printf("Witaj. Wybierz wariant wprowadzenia liczby zespolonej: \n");
    printf("- 1 - postac kartezjanska \n- 2 - postac trygonometryczna\n");

    do{
        wariant = getchar();

    }while(wariant != '1' && wariant != '2');

    wczytanie(wariant, &liczba); //uzycie procedur
    wypisanie(wariant, &liczba);
    przeliczanie(wariant, &liczba);

    return 0;
}

void wczytanie(char wariant, struct zespolona *liczba) // definicja procedury wczytujacej liczbe zespolona od uzytkownika
{
    if(wariant == '1'){ // wczytanie liczbv w postaci kartezjanskiej
        printf("Podaj czesc rzeczywista liczby zespolonej: \n");
        scanf("%f", &liczba->Re);
        printf("Podaj czesc urojona liczby zespolonej: \n");
        scanf("%f", &liczba->Im);
    }
    else{ // wczytanie liczbv w postaci trygonometrycznej
        printf("Podaj modul liczby zespolonej: \n");
        scanf("%f", &liczba->modul);
        printf("Podaj wartosc argumentu (w katach) liczby zespolonej: \n");
        scanf("%f", &liczba->Arg);
    }

    return;
};


void wypisanie(char wariant, struct zespolona *liczba) // definicja procedury wypisujacej liczbe zespolona
{

    if(wariant == '1'){ // wypisanie liczby w postaci kartezjanskiej
        printf("Podana przez Ciebie liczba zespolona: %.1f + %.1fi\n\n", liczba->Re, liczba->Im);
    }
    else{ // wypisanie liczy w postaci trygonometrycznej
        printf("Podana przez Ciebie liczba zespolona: |%.1f|(cos(%.1f) + isin(%.1f))\n\n", liczba->modul, liczba->Arg, liczba->Arg);
    }

    return;
};


void przeliczanie(char wariant, struct zespolona *liczba) // definicja procedury przeliczajacej liczbe zespolona
{
    if(wariant == '1'){ // wariant przeliczajacy postac kartezjanska do trygonometrycznej

        liczba->modul = sqrtf(powf(liczba->Re, 2) + powf(liczba->Im, 2)); // wyliczenie modulu zgodnie ze wzorem |z| = pierw(a^2 + b^2)

        if(liczba->Im >= 0 && liczba->modul != 0){
            float tmp = acosf(liczba->Re/liczba->modul); // wyliczenie argumentu i zapisanie w radianach
            liczba->Arg = (tmp * 180) / PI; // przeliczenie agrumentu na katy
        }
        else{
            float tmp = (-1) * acosf(liczba->Re/liczba->modul); // wyliczenie argumentu i zapisanie w radianach
            liczba->Arg = ((tmp * 180) / PI); // przeliczenie agrumentu na katy
        }
        printf("Twoja liczba przeksztalcona do postaci trygonometrycznej(argument podany w katach): |%.1f|(cos(%.1f) + isin(%.1f))\n\n", liczba->modul, liczba->Arg, liczba->Arg);
    }
    else{ // wariant przeliczajacy postac trygonometryczna do kartezjanskiej

        float kat_na_radian = (liczba->Arg * PI) / 180; // zamiana argumentu z kata na radiany

        liczba->Re = liczba->modul * cosf(kat_na_radian); // wyliczenie liczby rzeczywistej zgodnie ze wzorem a = |z|cos(fi)
        liczba->Im = liczba->modul * sinf(kat_na_radian); // wyliczenie liczby urojonej zgodnie ze wzorem b = |z|sin(fi)

        printf("Twoja liczba przeksztalcona do postaci kartezjanskiej: %.1f + %.1fi\n\n", liczba->Re, liczba->Im);

    }

    return;
};
